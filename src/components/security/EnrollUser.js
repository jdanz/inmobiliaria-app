import React, { Component } from 'react';
import { Container, Avatar, Typography, Grid, TextField, Button } from '@material-ui/core';
import LockOutLineIcon from '@material-ui/icons/LockOutlined';
import { compose } from 'recompose';
import { consumerFirebase } from '../../server';

const style = {
    paper: {
        marginTop: 9,
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        margin: 9,
        backgroundColor: "#e53935"
    },
    form: {
        width: "100%",
        marginTop: 10
    },
    submit: {
        marginTop: 15,
        marginBottom: 20
    }
}

//Setea los valores una vez, enviado el formulario
const userInitial = {
    name: '',
    last_name: '',
    email: '',
    password: ''

}
class EnrollUser extends Component {
    //  Variable tipo estado valores del formulario 
    state = {
        firebase: null,
        user: {
            name: '',
            last_name: '',
            email: '',
            password: ''
        }
    }

    //Metodo propio de react para obtener los datos firebase cuando retorna null like break
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.firebase === prevState.firebase) {
            return null;
        }
        // Cambia el state del componente
        return {
            firebase: nextProps.firebase
        }
    }

    // Funcion OnChange para asignar valores del formulario
    onChange = e => {
        let user = Object.assign({}, this.state.user);
        user[e.target.name] = e.target.value;
        this.setState({
            user: user
        })
    }

    //Funcion que evita recargar pantalla e imprime el log del usuario registrado
    userRegister = e => {
        e.preventDefault();
        console.log('Nombre usuario', this.state.user);
        const { user, firebase } = this.state;

        //Interaccion con instancia firebase
        firebase.db
            //Valida la existencia en la base de firebase
            .collection("Users")
            //Agrega el json user
            .add(user)
            //Respuesta firebase
            .then(userAfter => {
                console.log("La insercion exitosa", userAfter);
                this.setState({
                    user:userInitial
                })
            })
            //Catch de errores
            .catch(error => {
                console.log('error', error);
            })
    }

    render() {
        return (
            <Container maxWidth="md">
                <div style={style.paper}>
                    <Avatar style={style.avatar}>
                        <LockOutLineIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Enroll User
                    </Typography>
                    <form style={style.form}>
                        <Grid container spacing={2}>
                            <Grid item md={6} xs={12}>
                                <TextField name="name" onChange={this.onChange} value={this.state.user.name} fullWidth label="Type name" />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <TextField name="last_name" onChange={this.onChange} value={this.state.user.last_name} fullWidth label="Type last name" />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <TextField name="email" onChange={this.onChange} value={this.state.user.email} fullWidth label="Type email" />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <TextField type="password" onChange={this.onChange} value={this.state.user.password} name="password" fullWidth label="Type password" />
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={6}>
                                <Button type="submit" onClick={this.userRegister} variant="contained" fullWidth size="large" color="primary" style={style.submit}>
                                    Registrar
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}

export default compose(consumerFirebase)(EnrollUser);