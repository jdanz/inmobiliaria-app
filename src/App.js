import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import './App.css';
import ListRealty from './components/views/ListRealty';
//import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import theme from './themes/theme';
import AppNavbar from './components/layout/AppNavbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import EnrollUser from './components/security/EnrollUser';


class App extends Component {
  render() {
    return (
      <Router>
        <MuiThemeProvider theme={theme}>
          <AppNavbar />
          <Grid container>
            <Switch>
              <Route path="/" exact component={ListRealty}></Route>
              <Route path="/auth/enrollUser/" exact component={EnrollUser}></Route>
            </Switch>
          </Grid>

        </MuiThemeProvider>
      </Router>
    )
  }
}

export default App;
