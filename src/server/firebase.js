import app from 'firebase/app';
import 'firebase/firestore';

//Configuracion de Firebase de pruebas
const firebaseConfig = {
    apiKey: "AIzaSyDnbmMVpVTkNBYY1fweT1BznHPUkKR2Dcg",
    authDomain: "home-prop.firebaseapp.com",
    databaseURL: "https://home-prop.firebaseio.com",
    projectId: "home-prop",
    storageBucket: "home-prop.appspot.com",
    messagingSenderId: "653554706273",
    appId: "1:653554706273:web:b9bbb2af71fdd2cfa18720",
    measurementId: "G-3ZN1FJ3FRN"
};

class Firebase {
    constructor() {
        app.initializeApp(firebaseConfig);
        this.db = app.firestore();
    }
}

export default Firebase;